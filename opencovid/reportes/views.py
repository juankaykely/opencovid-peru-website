from django.shortcuts import render
from django.http import HttpResponse

from .models import ReportPage

from config.constants import REGIONES, NACIONAL

def find_report_dynamically_view(request, the_slug):
    r = ReportPage.objects.get(slug=the_slug)    
    return render(request, r.html_report_template, {
        'report_page' : r, 
        'REGIONES' : REGIONES,
        'NACIONAL' : NACIONAL,
    })
from reportes.models import ReportPage

def reports(request):
    return { 'reports' : ReportPage.objects.filter(published_at__isnull=False) }

from django.db import models
from django.utils import timezone

from solo.models import SingletonModel


class DashboardSummary(SingletonModel):
    '''
    Los valores que se enseñan en la página principal.
    La tabla sólo tiene un record.
    '''

    muertes = models.IntegerField(blank=False, null=False, default=0, 
        help_text='Muertes en todo el Perú (DIRESA)')
    muertes_fecha_actualizado = models.DateTimeField(blank=False, null=False, 
        default=timezone.now, help_text='Fecha de actualización de muertes')
    
    casos_activos = models.IntegerField(blank=False, null=False, default=0, 
        help_text='Casos Activos en todo el Perú')
    casos_activos_actualizado = models.DateTimeField(blank=False, null=False,
        default=timezone.now, help_text='Fecha de actualización de casos activos')

    muertes_sinadef = models.IntegerField(blank=False, null=False, default=0, 
        help_text='Estimación de muertes por COVID-19 (SINADEF)')
    muertes_sinadef_actualizado = models.DateTimeField(blank=False, null=False,
        default=timezone.now, help_text='Fecha de actualización de estimación de SINADEF')

    class Meta:
        verbose_name = "Dashboard Summary"

    def toDict(self):
        dictionary = {}
        for field in self._meta._get_fields():
            dictionary[field.attname] = self.__getattribute__(field.attname)
        return dictionary
        

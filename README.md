# OpenCovid-Peru.com Website

# Running locally
```bash
export DJANGO_SETTINGS_MODULE=opencovid.settings.local
pip install -r requirements/local.txt
cd opencovid
python manage.py migrate
python manage.py runserver_plus
```

# Deployment
```bash
gcloud config set account <your email>
gcloud auth login
gcloud config set project opencovid-peru
```

# Building and sending docker images to cloud run
```bash
gcloud builds submit --tag gcr.io/opencovid-peru/opencovid
gcloud run deploy opencovid --image gcr.io/opencovid-peru/opencovid --platform managed
```

# Setting up / Installing for the first time in Google Cloud
```bash
gcloud config set account <your email>
gcloud auth login
gcloud config set project opencovid-peru

# set some variables
PROJECT_ID=$(gcloud config get-value core/project)
REGION=us-east4

# set region to us-east4 (lowest latency to Perú)
gcloud config set run/region $REGION

# enable APIs
gcloud services enable \
  run.googleapis.com \
  sql-component.googleapis.com \
  sqladmin.googleapis.com \
  compute.googleapis.com \
  cloudbuild.googleapis.com \
  secretmanager.googleapis.com

# add quotas for auth
gcloud auth application-default login

# create a storage bucket for static (public) and another
# one for media(not public) and set cors headers
# the json file with the header info needs to be like this one:
# https://stackoverflow.com/questions/55226101/how-to-set-multiple-origins-in-the-cors-json-file-with-google-cloud-storage

GS_BUCKET_NAME_STATIC=${PROJECT_ID}-static
gsutil mb -l ${REGION} gs://${GS_BUCKET_NAME_STATIC}
gsutil cors set cors-bucket-config.json gs://${GS_BUCKET_NAME_STATIC}

GS_BUCKET_NAME_MEDIA=${PROJECT_ID}-media
gsutil mb -l ${REGION} gs://${GS_BUCKET_NAME_MEDIA}
gsutil cors set cors-bucket-config.json gs://${GS_BUCKET_NAME_MEDIA}

# create Cloud SQL micro instance for the DB
gcloud sql instances create opencovid-instance --project $PROJECT_ID \
  --database-version POSTGRES_12 --tier db-f1-micro --region $REGION
gcloud sql databases create opencovid --instance opencovid-instance
DJPASS="$(cat /dev/urandom | LC_ALL=C tr -dc 'a-zA-Z0-9' | fold -w 30 | head -n 1)" #save password in environment file
DJUSER=<your_django_user>
gcloud sql users create $DJUSER --instance opencovid-instance --password $DJPASS

##############
# create configuration file 'variables.env' with configuration settings
# you can look at variables.sample.env as an example
##############

# create a secret in SecretManager to store the environment file,
# load it into SecretManager
gcloud secrets create application_settings --replication-policy automatic
gcloud secrets versions add application_settings --data-file variables.env # update env variables

# ...and allow Cloud Run to read the secret and connect to Cloud SQL
export PROJECTNUM=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export CLOUDRUN=${PROJECTNUM}-compute@developer.gserviceaccount.com

gcloud secrets add-iam-policy-binding application_settings \
  --member serviceAccount:${CLOUDRUN} --role roles/secretmanager.secretAccessor

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${CLOUDRUN} --role roles/cloudsql.client

# ...and allow Cloud Build to read the secret and connect to Cloud SQL
export PROJECTNUM=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export CLOUDBUILD=${PROJECTNUM}@cloudbuild.gserviceaccount.com

gcloud secrets add-iam-policy-binding application_settings \
  --member serviceAccount:${CLOUDBUILD} --role roles/secretmanager.secretAccessor

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${CLOUDBUILD} --role roles/cloudsql.client

```
